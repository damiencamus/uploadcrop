/**
 * @project     NodevoCropHTML5
 * @date        12.10.28
 * @author      D.CAMUS - Nodevo <dcamus@nodevo.com>
 * @link        www.nodevo.com
 * 
 * JS File : control "Drag & Drop" and "Crop options"
 * 
 * @todo        No html5 support browser
 * @todo        Cleaning
 */


// On document Ready
$(function(){
    // Check if browser supports <input type=range/>
    var i = document.createElement("input");
    i.setAttribute("type", "range");
    var rangeNotSupported = (i.type === "text");
    
    // If browser doesn't support <input type=range/>
    // display block "no html5 supported"
    if(rangeNotSupported) {
        $('#html5options').hide();
        $('#nohtml5options').show();
    } else {
        $('#nohtml5options').hide();
        $('#html5options').show();
    }

    // Enable DropZone & DragDrop events
    var dropZoneElement = document.getElementById("dropZone");

    dropZoneElement.addEventListener('dragleave', onDragLeave, false);
    dropZoneElement.addEventListener('dragenter', onDragEnter, false);
    dropZoneElement.addEventListener('dragover', onDragOver, false);
    dropZoneElement.addEventListener('drop', onDrop, false);
});  

// Drag & Drop events
function onDragLeave(event) {
    event.preventDefault();
    event.stopPropagation();
    $('#dropZone').removeClass('hover');            
}

function onDragEnter(event) {
    event.preventDefault();
    event.stopPropagation();        
    $('#dropZone').addClass('hover');
}

function onDragOver(event) {
    event.preventDefault();
    event.stopPropagation();
    event.dataTransfer.effectAllowed= "copy";
    event.dataTransfer.dropEffect = "copy";
    $('#dropZone').addClass('hover');
}

function onDrop(event) {
    event.preventDefault();
    event.stopPropagation();
    $('#dropZone').removeClass('hover');
    onFilesSelected(event.dataTransfer.files);
}

// After selecting file
var jcrop_api, boundx, boundy;
function onFilesSelected(files) {
    var dropZoneElement = $("#dropZone");            
    for (var i=0; i<files.length; i++) {
         if (files[i].type.indexOf('image')==0) {
            var img = document.createElement("img");
            dropZoneElement.empty();                     
            dropZoneElement.html(img);
            uploadFile(files[i]);
            $('#submitcrop').hide();
            $('#loader1').show();$('#loader2').show();
            var reader = new FileReader();
            reader.onload = function (evt) {                   
                img.src = evt.target.result; 
               
                // Jcrop ! How easy is this??
                $('#dropZone > img').Jcrop({
                    onChange: updatePreview,
                    onSelect: updatePreview,
                    boxWidth: 500, 
                    boxHeight: 400,
                    onSelect: updateCoords
                },function(){
                    // Use the API to get the real image size
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    // Store the API in the jcrop_api variable
                    jcrop_api = this;
                });
                                          
            };
             reader.readAsDataURL(files[i]);
         } else { 
            alert('File type mismatch, please upload image.'); 
            $('#loader1').hide();$('#loader2').hide();
            $('#submitcrop').show();
         }
    }
}

/* Crop Preview 
 * @todo aspect ratio
 */
function updatePreview(c)
{
    if (parseInt(c.w) > 0)
    {
      
      var rx = 200 / c.w;
      var ry = 150 / c.h;

      $('#preview').css({
        width: Math.round(rx * boundx) + 'px',
        height: Math.round(ry * boundy) + 'px',
        marginLeft: '-' + Math.round(rx * c.x) + 'px',
        marginTop: '-' + Math.round(ry * c.y) + 'px'
      });
    }
}

/* Ajax upload file 
 * @todo loader */
function uploadFile(file) 
{
    
    var formData = new FormData();
    formData.append('file',file);
    
    $.ajax({
        url: 'includes/uploadfile.php',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
            $('#src').val(data);
            $('#preview').attr('src', 'userfiles/'+data);
            $('#preview').css('display','');
            $('#submitcrop').show();
            $('#loader1').hide();$('#loader2').hide();
        }
    });       
    /*
    var xhr = new XMLHttpRequest();
    //on s'abonne à l'événement progress pour savoir où en est l'upload
    //xhr.upload.addEventListener("progress",  onUploadProgress, false);
    xhr.open("POST", "includes/uploadfile.php", true);

    // on s'abonne à tout changement de statut pour détecter
    // une erreur, ou la fin de l'upload
    //xhr.onreadystatechange = onStateChange; 

    xhr.setRequestHeader("Content-Type", "multipart/form-data");
    xhr.setRequestHeader("X-File-Name", file.name);
    xhr.setRequestHeader("X-File-Size", file.size);
    xhr.setRequestHeader("X-File-Type", file.type);

    xhr.send(formData);
    */
}

// Update crop coords
function updateCoords(c)
{
	$('#x').val(c.x);
	$('#y').val(c.y);
	$('#w').val(c.w);
	$('#h').val(c.h);

    $('#rheight').attr('max',Math.round(c.h));
    $('#rheight').attr('value',Math.round(c.h));        

    $('#rwidth').attr('max',Math.round(c.w));
    $('#rwidth').attr('value',Math.round(c.w));

    onUpdateRange(Math.round(c.h),'h');
    onUpdateRange(Math.round(c.w),'w')
}

/* Check crop coords before croping */
function checkCoords()
{		
    $('#nw').val($('#rwidth').attr('value'));        
    $('#nh').val($('#rheight').attr('value'));
    if (parseInt($('#w').val())) return true;
	alert('Please select a crop region then press submit.');
	return false;
}

/* Sliders range updating */
function onUpdateRange(v,field)
{
	$('#t'+field).html(v);

    if (field=="h") {
        reduc = ($('#rheight').attr('max') / $('#rheight').attr('value'));
        newwidth = Math.round($('#w').val() / reduc);
	    $('#tw').html(newwidth);
        $('#rwidth').attr('value',newwidth);
    }  

    if (field=="w") {
        reduc = ($('#rwidth').attr('max') / $('#rwidth').attr('value'));
        newheight = Math.round($('#h').val() / reduc);
	    $('#th').html(newheight);
        $('#rheight').attr('value',newheight);
    }      
}



